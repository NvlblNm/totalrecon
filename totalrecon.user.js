// ==UserScript==
// @id           iitc-plugin-totalrecon@wintervorst
// @name         IITC plugin: Total Recon
// @category     Layer
// @version      1.6.2
// @namespace    https://gitlab.com/AlfonsoML/totalrecon/
// @downloadURL  https://gitlab.com/AlfonsoML/totalrecon/raw/master/totalrecon.user.js
// @homepageURL  https://gitlab.com/AlfonsoML/totalrecon/
// @description  Place markers on the map for possible candidates, submitted candidates, rejected candidates and succesful candidates.
// @match        https://intel.ingress.com/*
// @grant none
// ==/UserScript==
/* forked from https://github.com/Wintervorst/iitc/raw/master/plugins/totalrecon/ */

/* eslint-env es6 */
/* eslint no-var: "error" */
/* globals L, map */
/* globals GM_info, $, dialog */

;function wrapper(plugin_info) {
	'use strict';

	// PLUGIN START ///////////////////////////////////////////////////////


	// Initialize the plugin
	const setup = function () {
		
setTimeout(function() {
	alert('Script Total Recon renamed to Wayfarer Planner and migrated to https://gitlab.com/AlfonsoML/wayfarer/, please update');
	window.open('https://gitlab.com/AlfonsoML/wayfarer/');
}, 1000);

	};

	// PLUGIN END //////////////////////////////////////////////////////////

	setup.info = plugin_info; //add the script info data to the function as a property
	// if IITC has already booted, immediately run the 'setup' function
	if (window.iitcLoaded) {
		setup();
	} else {
		if (!window.bootPlugins) {
			window.bootPlugins = [];
		}
		window.bootPlugins.push(setup);
	}
}
// wrapper end

(function() {
	const plugin_info = {};
	if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) {
		plugin_info.script = {
			version: GM_info.script.version,
			name: GM_info.script.name,
			description: GM_info.script.description
		};
	}

	// Greasemonkey. It will be quite hard to debug
	if (typeof unsafeWindow != 'undefined' || typeof GM_info == 'undefined' || GM_info.scriptHandler != 'Tampermonkey') {
		// inject code into site context
		const script = document.createElement('script');
		script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(plugin_info) + ');'));
		(document.body || document.head || document.documentElement).appendChild(script);
	} else {
		// Tampermonkey, run code directly
		wrapper(plugin_info);
	}
})();