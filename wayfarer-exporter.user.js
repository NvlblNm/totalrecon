// ==UserScript==
// @name         Wayfarer Exporter
// @version      0.2.1
// @description  Export nominations data from Wayfarer to TotalRecon
// @namespace    https://gitlab.com/AlfonsoML/totalrecon/
// @downloadURL  https://gitlab.com/AlfonsoML/totalrecon/raw/master/wayfarer-exporter.user.js
// @homepageURL  https://gitlab.com/AlfonsoML/totalrecon/
// @match        https://wayfarer.nianticlabs.com/*
// ==/UserScript==

/* globals unsafeWindow */
/* eslint-env es6 */
/* eslint no-var: "error" */

setTimeout(function() {
	alert('Script Wayfarer Exporter migrated to https://gitlab.com/AlfonsoML/wayfarer/blob/master/exporter.md, please update');
	window.open('https://gitlab.com/AlfonsoML/wayfarer/blob/master/exporter.md');
}, 1000);


